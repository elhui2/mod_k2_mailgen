<?php
/**
 * K2 Mailer Generator
 * Generate well formed html with the k2 articles to send a mailing list
 * 
 * @author     Daniel Huidobro http://elhui2.info <daniel.hui287@gmail.com>
 * @package    elhui2.joomla.modules
 * @subpackage modules
 * @license    https://www.gnu.org/copyleft/gpl.html GNU/GPL V3 or later.
 * @link       http://joomla.elhui2.info/mailgen
 * @version    0.3 BETA
 */

require_once dirname(__FILE__) . '/helper.php';
require JModuleHelper::getLayoutPath('mod_k2_mailgen');