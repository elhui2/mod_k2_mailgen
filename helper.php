<?php

/**
 * modK2Mailgen
 * Helper class for module K2 Mailer Generator
 * 
 * @author     Daniel Huidobro http://elhui2.info <contacto@elhui2.info>
 * @package    elhui2.joomla.modules
 * @subpackage modules
 * @license    https://www.gnu.org/copyleft/gpl.html GNU/GPL V3 or later.
 * @link       http://joomla.elhui2.info/mailgen
 * @version    0.3 BETA
 */
require_once(JPATH_SITE . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_k2' . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'route.php');

class modK2Mailgen {

    private $params;

    public function __construct($params) {
        $this->params = $params;
    }

    /**
     * sortItems
     * 
     * @param string $period Period to generate the newsletter
     * @param string $featuredItems Filter to read the featured items or not
     * @param string $orderHits Order of items based in views
     * @param int $numberNotes Number of items in your newsletter
     * @return boolean|array False if don't have items or array with data
     */
    public function sortItems($period, $featuredItems, $orderHits, $numberNotes) {
        //The newsletter ere generated every sunday
        if (date('N') != 2) {
            return FALSE;
        }

        //Create SQL Query
        $query = 'SELECT a.id AS id,a.catid AS catid,a.title AS title,a.introtext AS introtext,a.hits AS hits,a.alias AS alias,c.alias AS catalias FROM #__k2_items AS a LEFT JOIN #__k2_categories AS c ON ( a.catid = c.id ) ';
        switch ($period) {
            case 'from_beginning':
                $query .= 'WHERE a.published = 1';
                break;
            case 'current_year':
                $query .= 'WHERE a.published = 1 AND (YEAR(NOW()) = YEAR(a.created) )';
                break;
            case 'past_year':
                $query .= 'WHERE a.published = 1 AND (YEAR(NOW()-1) = YEAR(a.created) )';
                break;
            case 'current_month':
                $query .= 'WHERE a.published = 1 AND (YEAR(NOW()) = YEAR(a.created)) AND ((MONTH(NOW()))=MONTH(a.created))';
                break;
            case 'past_month':
                $query .= 'WHERE a.published = 1 AND (YEAR(NOW()) = YEAR(a.created)) AND ((MONTH(NOW())-1)=MONTH(a.created))';
                break;
            case 'current_week':
                $query .= 'WHERE a.published = 1 AND (YEAR(NOW()) = YEAR(a.created)) AND ((WEEK(NOW()))=WEEK(a.created))';
                break;
            case 'past_week':
                $query .= 'WHERE a.published = 1 AND (YEAR(NOW()) = YEAR(a.created)) AND ((WEEK(NOW())-1)=WEEK(a.created))';
                break;
        }

        if ($featuredItems == 'featured') {
            $query .= ' AND (a.featured=1)';
        } else
        if ($featuredItems == 'featured') {
            $query .= ' AND (a.featured=0)';
        }

        $query .= ' ORDER BY a.hits ' . $orderHits . ' LIMIT ' . $numberNotes;

        $db = JFactory::getDBO();

        $db->setQuery($query);
        $articles = $db->loadObjectList();

        $response = array();
        foreach ($articles as $article) {

            $link = array();
            $link['title'] = $article->title;
            $introText = preg_match('/<p>(.*?)<\/p>/s', $article->introtext, $matches);
            $link['introtext'] = $matches[1];
            $link['image'] = '/media/k2/items/cache/' . md5('Image' . $article->id) . '_M.jpg';
            $link['link'] = JRoute::_(K2HelperRoute::getItemRoute($article->id . ':' . urlencode($article->alias), $article->catid . ':' . urlencode($article->catalias)));

            array_push($response, $link);
        }

        if (count($response) == 0) {
            return FALSE;
        }

        // Save as json
        file_put_contents(JPATH_BASE . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'mod_k2_mailgen' . DIRECTORY_SEPARATOR . 'archive' . DIRECTORY_SEPARATOR . 'newsletter.json', json_encode($response));

        //Return array with the data
        return $response;
    }

    public function oneLight() {

        $config = JFactory::getConfig();
        $newsletter = file_get_contents(JPATH_BASE . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'mod_k2_mailgen' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'one_column_light.html');
        $data = file_get_contents(JPATH_BASE . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'mod_k2_mailgen' . DIRECTORY_SEPARATOR . 'archive' . DIRECTORY_SEPARATOR . 'newsletter.json');
        $data = json_decode($data);
        
        $img = $this->params->get('img_url');
        
        if (!empty($img)) {
            $img = $this->params->get('img_url');
        } else {
            $img = rtrim(JURI::base(), '/');
        }
        
        $baseUrl = rtrim(JURI::base(), '/');
        $newBody = '';
        foreach ($data as $item) {
            $newBody .= '<tr>'
                    . '<td>'
                    . '<h2 style="background-color: {BACKGROUNDCOLOR}; padding: 5px 5px; margin:0;"><a href="' . $baseUrl . $item->link . '#GA" style="color: {COLORTEXT}; text-decoration: none">' . $item->title . '</a></h2>'
                    . '<a href="' . $baseUrl . $item->link . '#GA"><img src="' . $img . $item->image . '" style="width: 100%"></a>'
                    . '<p style="padding: 5px; margin: 0;">' . $item->introtext . '</p>'
                    . '</td>'
                    . '</tr>';
        }
        $newsletter = str_replace('{DATAROWS}', $newBody, $newsletter);
        $newsletter = str_replace('{SITENAME}', $config->get('sitename'), $newsletter);
        $newsletter = str_replace('{BASEURL}', $baseUrl, $newsletter);
        $newsletter = str_replace('{LOGOURL}', $this->params->get('logo_url'), $newsletter);
        $newsletter = str_replace('{TWITTERURL}', $this->params->get('twitter_url'), $newsletter);
        $newsletter = str_replace('{FACEBOOKURL}', $this->params->get('facebook_url'), $newsletter);
        $newsletter = str_replace('{COLORTEXT}', $this->params->get('header_color'), $newsletter);
        $newsletter = str_replace('{BACKGROUNDCOLOR}', $this->params->get('header_bgcolor'), $newsletter);
        $newsletter = str_replace('{LEGALTEXT}', $this->params->get('legal_text'), $newsletter);

        echo $newsletter;
        die();
    }

}
