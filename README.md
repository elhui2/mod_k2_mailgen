# K2 Mailer Generator.
#### Version. 0.3 BETA
##### Autor. Daniel Huidobro <contacto@elhui2.info> http://elhui2.info
##### Licencia. GNU GPL Version 3 o superior. http://www.gnu.org/licenses/gpl-3.0.html
##### http://joomla.elhui2.info/mailgen

K2 Mailer Generator es un módulo para Joomla 2.5 y 3.X crea una plantilla HTML utilizando los datos del componente K2. Con el propósito de utilizarla para el envío de un boletín mediante "servicios de entrega masivos".

### Filtros:
- Mas visitados.
- Menos visitados.
- Destacadas.
- No Destacadas.
  
### Periodos:
- Total.
- Año Actual.
- Año Anterior.
- Mes actual.
- Mes anterior.
- Semana actual.
- Semana anterior.

### Otros:
- Numero de notas
- CDN URL
- Logo URL
- Facebook URL
- Twitter URL
- Header Color