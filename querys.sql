/**
 * querys.sql
 * Consultas a la base de datos para el modulo K2 Mailgen
 * 
 * @author     Daniel Huidobro http://elhui2.info <daniel.hui287@gmail.com>
 * @package    elhui2.joomla.modules
 * @subpackage modules
 * @license    https://www.gnu.org/copyleft/gpl.html GNU/GPL V3 or later.
 * @link       http://joomla.elhui2.info/mailgen
 * @version    0.2.5
 * @internal   Sustituye el prefijo (pfx) de la tabla por el prefijo de configuration.php el order y el limit cambian segun los parametros del modulo
 */

-- From Beginning
SELECT
    a.id AS id,
    a.catid AS catid,
    a.title AS title,
    a.introtext AS introtext,
    a.hits AS hits,
    a.alias AS alias,
    c.alias AS catalias
FROM 
    pfx_k2_items AS a 
    LEFT JOIN pfx_k2_categories AS c ON ( a.catid = c.id ) 
    WHERE a.published = 1 ORDER BY a.hits DESC LIMIT 10

-- Current Year
SELECT
    a.id AS id,
    a.catid AS catid,
    a.title AS title,
    a.introtext AS introtext,
    a.hits AS hits,
    a.alias AS alias,
    c.alias AS catalias
FROM 
    pfx_k2_items AS a 

    LEFT JOIN pfx_k2_categories AS c ON ( a.catid = c.id ) 
    WHERE a.published = 1 AND (YEAR(NOW()) = YEAR(a.created) ) ORDER BY a.hits DESC LIMIT 10

-- Past Year
SELECT
    a.id AS id,
    a.catid AS catid,
    a.title AS title,
    a.introtext AS introtext,
    a.hits AS hits,
    a.alias AS alias,
    c.alias AS catalias
FROM 
    pfx_k2_items AS a 

    LEFT JOIN pfx_k2_categories AS c ON ( a.catid = c.id ) 
    WHERE a.published = 1 AND (YEAR(a.created) = (YEAR(NOW())-1) ) ORDER BY a.hits DESC LIMIT 10

-- Current month
SELECT
    a.id AS id,
    a.catid AS catid,
    a.title AS title,
    a.introtext AS introtext,
    a.hits AS hits,
    a.alias AS alias,
    c.alias AS catalias
FROM 
    pfx_k2_items AS a 
    LEFT JOIN pfx_k2_categories AS c ON ( a.catid = c.id ) 
    WHERE a.published = 1 AND (YEAR(NOW()) = YEAR(a.created)) AND ((MONTH(NOW()))=MONTH(a.created)) ORDER BY a.hits DESC LIMIT 10;

-- Past month
SELECT
    a.id AS id,
    a.catid AS catid,
    a.title AS title,
    a.introtext AS introtext,
    a.hits AS hits,
    a.alias AS alias,
    c.alias AS catalias
FROM 
    pfx_k2_items AS a 
    LEFT JOIN pfx_k2_categories AS c ON ( a.catid = c.id ) 
    WHERE a.published = 1 AND (YEAR(NOW()) = YEAR(a.created)) AND ((MONTH(NOW())-1)=MONTH(a.created)) ORDER BY a.hits DESC LIMIT 10;

-- Current Week
SELECT
    a.id AS id,
    a.catid AS catid,
    a.title AS title,
    a.introtext AS introtext,
    a.hits AS hits,
    a.alias AS alias,
    c.alias AS catalias
FROM 
    pfx_k2_items AS a 
    LEFT JOIN pfx_k2_categories AS c ON ( a.catid = c.id ) 
    WHERE a.published = 1 AND (YEAR(NOW()) = YEAR(a.created)) AND ((WEEK(NOW()))=WEEK(a.created)) ORDER BY a.hits DESC LIMIT 10;

-- Past Week

SELECT
    a.id AS id,
    a.catid AS catid,
    a.title AS title,
    a.introtext AS introtext,
    a.hits AS hits,
    a.alias AS alias,
    c.alias AS catalias
FROM 
    pfx_k2_items AS a 
    LEFT JOIN pfx_k2_categories AS c ON ( a.catid = c.id ) 
    WHERE a.published = 1 AND (YEAR(NOW()) = YEAR(a.created)) AND ((WEEK(NOW())-1)=WEEK(a.created)) ORDER BY a.hits DESC LIMIT 10;